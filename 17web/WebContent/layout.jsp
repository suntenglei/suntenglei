<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
    <title></title>
    <style>
        body{
            width: 100%;
        }
        .clear{
            clear: both;
        }
        .Main{
            width: 80%;
            height: 600px;
            line-height: 600px;
            text-align: center;
            background: #6b0b6d0f;
        }
        .top,.Main,.Bottom{
            float: right;
        }
    </style>
</head>
<body>

        <jsp:include page="fenye/top.jsp" flush="false"></jsp:include>

        <div class="Main">Main</div>

        <jsp:include page="fenye/footer.jsp" flush="false"></jsp:include>


        <jsp:include page="fenye/left.jsp" flush="false"></jsp:include>


</body>
</html>