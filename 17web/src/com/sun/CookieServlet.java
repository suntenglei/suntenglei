package com.sun;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/CookieServlet")
public class CookieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		?username=admin&password=123&remember=true&submit=登录
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		boolean flag = false;
		if(request.getParameter("remember") != null) {
			flag = Boolean.parseBoolean(request.getParameter("remember"));
		}
		if(flag == true) {
			Cookie cookie = new Cookie(username,password);
			cookie.setMaxAge(24*60*60);
			cookie.setHttpOnly(true);
			response.addCookie(cookie);
		}
		request.getSession().setAttribute("user", username);
		response.sendRedirect("IndexServlet");
//		request.getRequestDispatcher().forward(request, response);		
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
