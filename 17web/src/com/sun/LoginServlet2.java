package com.sun;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/LoginServlet2")
public class LoginServlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		boolean flag = false;
		Cookie[] cookies = request.getCookies();
		for (Cookie cookie : cookies) {
			if(username.equals(cookie.getName())) {
				flag = true;
				request.setAttribute("message","已经登录过了");
				request.setAttribute("username", username);
			}
		}
		if(flag) {
			request.getRequestDispatcher("WEB-INF/index.jsp").forward(request, response);
		}else {
			response.sendRedirect("login2.html");
		}
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
