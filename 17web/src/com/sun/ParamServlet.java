package com.sun;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ParamServlet
 */
@WebServlet("/ParamServlet")
public class ParamServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ParamServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String username = request.getParameter("username");

		System.out.println(username);

		String password = request.getParameter("password");

		if ("123".equals(password)) {
			System.out.println(".....");
		}

		System.out.println(password);

		String age = request.getParameter("age"); // null和空串 问题

		System.out.println("age:" + age);
		response.setCharacterEncoding("utf-8");
		response.getWriter().print("<head><meta charset=\"UTF-8\"></head>");
		response.getWriter().append("姓名:" + username).append("<br/>password:" + password).append("<br/>age:" + age);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		
		String username = request.getParameter("username");

		System.out.println(username);

		String password = request.getParameter("password");


		System.out.println(password);

		String age = request.getParameter("age"); // null和空串 问题

		System.out.println("age:" + age);
		
		System.out.println("id:" + request.getParameter("id"));
		
		String ball = request.getParameter("ball");//只能获取到第一个
		System.out.println(ball);
		String[] balls = request.getParameterValues("ball");
		System.out.println(balls);
		for (String b:balls) {
			System.out.println(b);
		}
		
		
		
		response.setCharacterEncoding("utf-8");
		response.getWriter().print("<head><meta charset=\"UTF-8\"></head>");
		response.getWriter().append("姓名:"+username).append("<br/>password:"+password)
		.append("<br/>age:"+age);
	}

}
