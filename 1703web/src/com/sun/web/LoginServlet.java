package com.sun.web;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
	
	/*protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}*/

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		/**
		 * request.getParameterNames()方法是将发送请求页面中form表单里所有具有name属性的表单对象获取(包括button).
		 * 返回一个Enumeration类型的枚举。
		 * 通过Enumeration的hasMoreElements()方法遍历.再由nextElement()方法获得枚举的值。
		 * 此时的值是form表单中所有控件的name属性的值。
		 * 最后通过request.getParameter()方法获取表单控件的value值。
		 */
		Enumeration<String> pNames = request.getParameterNames();
		while(pNames.hasMoreElements()){
		    String name=(String)pNames.nextElement();
		    String value=request.getParameter(name);
		    System.out.println(name + "=" + value);
		}
		response.setContentType("text/html;charset=utf-8");
		response.getWriter().append("用户名："+username+"<br />").append("密&nbsp;&nbsp;&nbsp;&nbsp;码："+password);
		/*request.getParameterValues("name")方法将获取所有form表单中name属性为"name"的值.该方法返回一个数组.遍历数组就可得到value值.

		String values = request.getParameterValues("name");
		for(String value : values){
		    System.out.println(value);
		}

		request.getParameterNames()的值是无序排列request.getParameterValues()是按照from表单的控件顺序排列.
	*/
	}

}
