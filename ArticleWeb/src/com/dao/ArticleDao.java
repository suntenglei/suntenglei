package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.entity.Article;
import com.util.DBUtil;

public class ArticleDao {

	private Connection conn;
	private PreparedStatement ps;
	private ResultSet rs;

	/**
	 * 查询所有的数据
	 * 列表页面
	 * @return
	 */
	public List<Article> selectAll() {
		List<Article> list = new ArrayList<Article>();
		Article article = null;
		try {
			conn = DBUtil.getConn();
			String sql = "select * from article";
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()) {
//				System.out.println("adfaf");
				article = new Article();
				article.setId(rs.getInt("id"));
				article.setTitle(rs.getString("title"));
				article.setIp(rs.getString("ip"));
				article.setCreateDate(rs.getDate("createDate"));
				article.setModifiedDate(rs.getDate("modifiedDate"));
				list.add(article);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			DBUtil.close(conn, ps, rs);
		}
		return list;
	}
	/**
	 * 根据ID进行查询
	 * 编辑页面
	 * @param id
	 * @return
	 */
	public Article selectOne(Integer id) {
		Article article = null;
		try {
			conn = DBUtil.getConn();
			String sql = "select * from article where id = ?";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			if(rs.next()) {
				article = new Article();
				article.setId(rs.getInt("id"));
				article.setTitle(rs.getString("title"));
				article.setContent(rs.getString("content"));
				article.setIp(rs.getString("ip"));
				article.setCreateDate(rs.getDate("createDate"));
				article.setModifiedDate(rs.getDate("modifiedDate"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			DBUtil.close(conn, ps, rs);
		}
		return article;
	}
	
	/**
	 * 添加页面
	 * @param article
	 * @return
	 */
	public int insertOne(Article article) {
		int row = 0;
		try {
			conn = DBUtil.getConn();
			String sql = "insert into article values(null,?,?,?,now(),now())";
			ps = conn.prepareStatement(sql);
			ps.setString(1, article.getTitle());
			ps.setString(2, article.getContent());
			ps.setString(3, article.getIp());
			row = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			DBUtil.close(conn, ps, rs);
		}
		return row;
	}
	/**
	 * 更新一条数据
	 * @param article
	 * @return
	 */
	public int updateOne(Article article) {
		int row = 0;
		try {
			conn = DBUtil.getConn();
			String sql = "update article set title=?,content=?,modifiedDate=now() where id =?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, article.getTitle());
			ps.setString(2, article.getContent());
			ps.setInt(3, article.getId());
			row = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			DBUtil.close(conn, ps, rs);
		}
		return row;
	}
	
	/**
	 * 根据id删除一条数据
	 * @param id
	 * @return
	 */
	public int deleteOne(int id) {
		int row = 0;
		try {
			conn = DBUtil.getConn();
			String sql = "delete from article where id =?";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			row = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			DBUtil.close(conn, ps, rs);
		}
		return row;
	}
	
}
