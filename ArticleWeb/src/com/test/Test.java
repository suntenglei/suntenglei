package com.test;


import java.util.List;

import com.dao.ArticleDao;
import com.entity.Article;

public class Test {

	
	@org.junit.Test
	public void selectAll() {
		ArticleDao ad = new ArticleDao();
		List<Article> list = ad.selectAll();
		System.out.println(list);
	}
	@org.junit.Test
	public void selectOne() {
		ArticleDao ad = new ArticleDao();
		Article article = ad.selectOne(1);
		System.out.println(article);
	}
	@org.junit.Test
	public void InsertOne() {
		ArticleDao ad = new ArticleDao();
		Article article = new Article();
		article.setTitle("dfadsf");
		article.setContent("fasdfasdfasdfasdfdsafsqwrerqwer");
		article.setIp("255.233.254.251");
		int row = ad.insertOne(article);
		System.out.println(row);
	}
	
	@org.junit.Test
	public void updateOne() {
		ArticleDao ad = new ArticleDao();
		Article article = new Article();
		article.setTitle("面向对象");
		article.setContent("oop");
		article.setId(2);
		int row = ad.updateOne(article);
		System.out.println(row);
	}
}
