package com.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class GetIP {
    public static String getLocalIPForCMD(){
        StringBuilder sb = new StringBuilder();
        String command = "cmd.exe /c ipconfig | findstr IPv4";  
        try {
            Process p = Runtime.getRuntime().exec(command);
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream())); 
            String line = null;
            while((line = br.readLine()) != null){
                line = line.substring(line.lastIndexOf(":")+2,line.length());
                sb.append(line+",");
            }
            br.close();
            p.destroy();
        } catch (IOException e) {
            e.printStackTrace();
        }  
        String ips = sb.toString();
        String[] split = ips.split(",");
        
        return split[0];
    }
}

