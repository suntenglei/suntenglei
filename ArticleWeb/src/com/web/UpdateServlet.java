package com.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.ArticleDao;
import com.entity.Article;


@WebServlet("/Update")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Integer id = Integer.parseInt(request.getParameter("id"));
		Article ar = new Article();
		ar.setId(id);
		ar.setTitle(request.getParameter("title"));
		ar.setContent(request.getParameter("content"));
		ArticleDao ad = new ArticleDao();
		int row = ad.updateOne(ar);
		if(row != 1) {
			request.setAttribute("meassge", "文章修改失败了");
			request.getRequestDispatcher("error.jsp").forward(request, response);
		}else {
			request.getSession().setAttribute("message", "文章修改成功");
			System.out.println(request.getSession().getAttribute("message")+"----------"+"修改");
			response.sendRedirect("ArticleList");
		}
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
