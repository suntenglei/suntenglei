package com.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.ArticleDao;
import com.entity.Article;


@WebServlet("/ArticleList")
public class ArticleList extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private ArticleDao ad = null;   

	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		ad = new ArticleDao();
		List<Article> selectAll = ad.selectAll();
		request.setAttribute("list", selectAll);
		HttpSession session = request.getSession();
		String message = (String) session.getAttribute("message");
		System.out.println(request.getSession().getAttribute("message"));
		if(message != null) {
			request.setAttribute("message", message);
			session.setMaxInactiveInterval(1);
		}
		request.getRequestDispatcher("article_list.jsp").forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		
		doGet(request, response);
	}

}
