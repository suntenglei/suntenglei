package com.web;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.ArticleDao;
import com.entity.Article;


@WebServlet("/Add")
public class AddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String title = request.getParameter("title");
		String content = request.getParameter("content");
		String ip = request.getParameter("ip");
		Article ar = new Article();
		ar.setTitle(title);
		ar.setContent(content);
		ar.setIp(ip);
		ArticleDao ad = new ArticleDao();
		int row = ad.insertOne(ar);
		if(row != 1) {
			request.setAttribute("meassge", "文章保存失败了");
			request.getRequestDispatcher("error.jsp").forward(request, response);
		}else {
			System.out.println("-------------");
			request.getSession().setAttribute("message", "文章添加成功");
			System.out.println(request.getSession().getAttribute("message")+"----------"+"保存");
			response.sendRedirect("ArticleList");
		}
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
