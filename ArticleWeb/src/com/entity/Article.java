package com.entity;

import java.io.Serializable;
import java.util.Date;

public class Article implements Serializable{

	private static final long serialVersionUID = -7436496715666463649L;
	
	private Integer id;
	private String title;
	private String content;
	private String ip;
	private Date createDate;
	private Date modifiedDate;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	@Override
	public String toString() {
		return "article [id=" + id + ", title=" + title + ", content=" + content + ", ip=" + ip + ", createDate="
				+ createDate + ", modifiedDate=" + modifiedDate + "]";
	}
	
	
	
	
}
