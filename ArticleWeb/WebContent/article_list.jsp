<%@page import="com.entity.Article"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>文章列表页面</title>
</head>
<script type="text/javascript">
<% String message = (String)request.getAttribute("message");%>
var message = "<%=message %>";
if(message != "null"){
	alert(message)
}
</script>
<body>
	<a href="article_add.jsp">添加文章</a>
	<table class="list" border="1px" >
		<tr align="center" height="40" >
			<td width="50">编号</td>
			<td width="200">文章标题</td>
			<td width="150">IP地址</td>
			<td width="150">创建时间</td>
			<td width="150">修改时间</td>
			<td width="100">操作</td>
		</tr>
		<tbody>
			<% 
				List<Article> lists = (List<Article>)request.getAttribute("list");
				
				if(lists != null){
					for(Article list : lists){
			%> 
			<tr align="center" height="40" >
			<td><%=list.getId() %></td>
			<td><%=list.getTitle() %></td>
			<td><%=list.getIp() %></td>
			<td><%=list.getCreateDate() %></td>
			<td><%=list.getModifiedDate() %></td>
			<td><a href="Edit?id=<%=list.getId() %>">修改</a>&nbsp;|&nbsp;<a href="Delete?id=<%=list.getId() %>">删除</a></td>
			</tr>
			
			<%
					}
				}
			%>
		</tbody>
	</table>
</body>
</html>