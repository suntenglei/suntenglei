<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>添加文章页面</title>

<%@ page import="com.util.GetIP" %>
<% String IP = GetIP.getLocalIPForCMD();%>
</head>
<body>
	<form action="Update" method="post">
		<div>
			文章标题：<input type="text" name="title" value="<%=request.getAttribute("title")%>">
		</div>
		<div>
			<span>文章内容：</span><br>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<textarea name="content" cols="100" rows="25"  value=""><%=request.getAttribute("content")%></textarea>
		</div>
		<div hidden="">
			<input type="text" name="id" value="<%=request.getAttribute("id")%>">
			<input type="text" name="ip" value="<%=IP%>">
		</div>
		<input type="reset" value="重置"> |
		<input type="submit" value="修改">
	</form>
</body>
</html>